function I=romberg(f,a,b,n)
        %%%%%%%%%%% ARGUMENTY  %%%%%%%%%%
% f - wzor funkcji ktora bedzie calkowana 
% a,b - przedzial calkowania
% n - ilosc probek wewnatrz <a,b>
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% <<<<<<<<<<<< ZWRACANE WARTOSCI <<<<<<<<<<<<<
% I - przyblizenie calki, 
% Podczas dzialania funkcja wyswietla kolejne  kolumny tablicy Romberga
% <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
%  Funkcja romberg implementuje calkowanie metoda Romberga
% W kodzie skryptu zostala uzyta kilkukrotnie komenda feval Uzywamy
% jej,gdy potrzebujemy do obliczenia wartosci funkcji przekazywanej
% jako parametr innej funkcji.
h=b-a; %zgodnie z wzorem h=(b-a)/n, poczatkowa wartosc n wynosi 1 
R(1,1)=h*(feval(f,a)+feval(f,b))/2; %T(1.0)=h*(1/1+1/2)
fprintf('%16.11f\n',R(1,1)); %wyswietlamy w pierwszym wierszu poczatkowe wartosci obliczen
m=1; %poczatkowa wartosc przedzialow
for i=1:n-1  %piewsza petla  
 h=h/2; %Co kazdy krok zmniejszamy h o polowe
 S=0; %tworzymy zmienna S,przypisujac jej na poczatek wartosc zero ktorej bedziemy uzywac w nastepnym kroku
 for j=1:m %Pierwsza petla odpowiada za ilosc wierszy okreslonych przez wartosc n
 %Kolejne dwie linijki kodu to dzialania zgodne z wzorem
 x=a+h*(2*j-1);
 S=S+feval(f,x);
 end
 R(i+1,1)=R(i,1)/2+h*S; %W tej i nastepnej linijce kodu obliczane i wyswietlane sa kolejne wartosci R dla pierwszej kolumny kazdego wiersza
 fprintf('%16.11f',R(i+1,1));
 m=2*m; %Podwajamy ilosc przedzialow dla kolejnych przyblizen
 for k=1:i %Ta petla odpowiada za wyswietlanie wartosci drugi wiersz=dwie kolumny, trzeci wiersz=trzy kolumny itd...
 R(i+1,k+1)=R(i+1,k)+(R(i+1,k)-R(i,k))/(4^k-1); %Zgodnie  z wzorem obliczamy kolejne wartosci R i je wyswietlamy
 fprintf('%16.11f',R(i+1,k+1));
 end
 fprintf('\n'); %Jesli wykona sie jedna petla(pierwszy wiersz), to przechodzimy do nastepnej zaczynajac od nowej linijki
end
I = R(i+1,k+1); %Wyswietlamy koncowa wartosc, aby porownac dzialanie z  funkcja quadl.(Z dokladnoscia taka sama jak w funkcji quadl)
 